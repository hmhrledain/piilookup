*This module allows to lookup SAMC USER_ID's against the Redshift DB to obtain PII data (first_name, last_name) currently not available in DOMO.*

**It only takes 3 steps:**

1. Choose (browse) any CSV file with a user_id column
2. Press Lookup button
3. Obtain result in right-pane and optionally save to a file

**Requirements:**

1. Be connected to HMH VPN
2. Have a 64-bit ODBC connection setup to the Redshift DB named HMH_Redshift_64

**To create standalone executable with pyinstaller:**
    
Pandas module has a dependency on numpy which installs a very large libray (MKL)
This will cause the executable to be around 600MB
To prevent this, as neither numpy and MKL are used here, we need to install a version of numpy that binds
with another opensource library called OpenBlas.

    From an Anaconda prompt:
    1- Install numpy with:
        conda install -c conda-forge numpy
    2- Add numpy as a pinned_package so that OpenBlas isn't downgraded to MKL :
        conda config --env --add pinned_packages conda-forge::numpy
    3- Install Pandas and remaining dependencies

    References:
    https://stackoverflow.com/questions/43886822/pyinstaller-with-pandas-creates-over-500-mb-exe/48846546#48846546
    https://github.com/conda-forge/numpy-feedstock/issues/84
    https://github.com/conda/conda/issues/7548

